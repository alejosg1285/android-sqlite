package com.alejosaag.projectsqldates;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.alejosaag.projectsqldates.Entities.Doctor;
import com.alejosaag.projectsqldates.Entities.Speciality;
import com.alejosaag.projectsqldates.Models.DDoctor;
import com.alejosaag.projectsqldates.Models.DSpeciality;

import java.util.ArrayList;

public class EditDoctor extends AppCompatActivity {
    EditText txtIdentification;
    EditText txtName;
    EditText txtLastName;
    Spinner cboSpeciallity;
    Button btnEdit;
    Button btnDelete;

    Doctor selectedDoctor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_doctor);

        selectedDoctor = (Doctor) getIntent().getSerializableExtra("selectedDoctor");

        txtIdentification = findViewById(R.id.txtIdentification);
        txtName = findViewById(R.id.txtName);
        txtLastName = findViewById(R.id.txtLastName);
        cboSpeciallity = findViewById(R.id.cboSpeciallity);
        btnEdit = findViewById(R.id.btnEdit);
        btnDelete = findViewById(R.id.btnDelete);

        loadSpeccialitySpinner();

        txtIdentification.setText(selectedDoctor.getIdentificacion());
        txtName.setText(selectedDoctor.getNombres());
        txtLastName.setText(selectedDoctor.getApellidos());

        cboSpeciallity.setSelection(getIndex(cboSpeciallity, selectedDoctor.getEspecialidad_id()));

        btnEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editDoctor();
            }
        });
        btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteDoctor();
            }
        });
    }

    private void loadSpeccialitySpinner() {
        DSpeciality dSpeciality = new DSpeciality(getBaseContext());

        ArrayList<Speciality> lstSpecialities = new ArrayList<>();

        lstSpecialities = dSpeciality.getSpecialities();

        ArrayAdapter<Speciality> adapter = new ArrayAdapter<Speciality>(getBaseContext(), android.R.layout.simple_spinner_dropdown_item, lstSpecialities);

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        cboSpeciallity.setAdapter(adapter);
    }

    private int getIndex(Spinner spinner, int id) {
        int index = 0;

        for (int i = 0; i < spinner.getCount(); i++) {
            if (((Speciality) spinner.getItemAtPosition(i)).getId() == id) {
                index = i;
                break;
            }
        }

        return index;
    }

    public void deleteDoctor() {
        DDoctor dDoctor = new DDoctor(getBaseContext());

        int idSelectedDoctor = selectedDoctor.getId();

        if (dDoctor.deleteDoctor(idSelectedDoctor) > 0) {
            Toast.makeText(getBaseContext(), "Medico eliminado", Toast.LENGTH_LONG).show();
            finish();
        } else {
            Toast.makeText(getBaseContext(), "Error al borrar el medico", Toast.LENGTH_LONG).show();
        }
    }

    public void editDoctor() {
        int idSelectedDoctor = selectedDoctor.getId();

        Doctor doctor = new Doctor();

        doctor.setId(idSelectedDoctor);
        doctor.setIdentificacion(txtIdentification.getText().toString());
        doctor.setNombres(txtName.getText().toString());
        doctor.setApellidos(txtLastName.getText().toString());
        doctor.setEspecialidad_id(((Speciality) cboSpeciallity.getSelectedItem()).getId());

        DDoctor dDoctor = new DDoctor(getBaseContext());

        if (dDoctor.updateDoctor(doctor) > 0) {
            Toast.makeText(getBaseContext(), "Registro editado", Toast.LENGTH_LONG).show();
            finish();
        } else {
            Toast.makeText(getBaseContext(), "Error al editar el registro", Toast.LENGTH_LONG).show();
        }
    }

    public void mainMenu(View view) {
        Intent intent = new Intent(getBaseContext(), MainActivity.class);
        startActivity(intent);
    }
}
