package com.alejosaag.projectsqldates;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.alejosaag.projectsqldates.Entities.Patient;
import com.alejosaag.projectsqldates.Models.DPatient;

public class TabAddPatient extends Fragment {
    Button btnAdd;
    EditText txtIdentification;
    EditText txtName;
    EditText txtLastName;
    View layout;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        layout = inflater.inflate(R.layout.tab_add_patient, container, false);

        txtIdentification = layout.findViewById(R.id.txtIdentificationPatient);
        txtName = layout.findViewById(R.id.txtNamePatient);
        txtLastName = layout.findViewById(R.id.txtLastNamePatient);

        btnAdd =  layout.findViewById(R.id.btnAddPatient);
        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                savePatient();
            }
        });

        return layout;
    }

    public void savePatient() {
        DPatient dPatient = new DPatient(getContext());
        Patient patient = new Patient();

        patient.setIdentificacion(txtIdentification.getText().toString());
        patient.setNombres(txtName.getText().toString());
        patient.setApellidos(txtLastName.getText().toString());

        String res = "";
        if (dPatient.insertPatient(patient) > 0)
            res = "Registro insertado";
        else
            res = "Registro no insertado";

        Toast.makeText(getContext(), res, Toast.LENGTH_SHORT).show();
    }
}
