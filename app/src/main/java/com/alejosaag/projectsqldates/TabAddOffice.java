package com.alejosaag.projectsqldates;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.alejosaag.projectsqldates.Entities.Office;
import com.alejosaag.projectsqldates.Models.DOffice;

public class TabAddOffice extends Fragment {
    Button btnAdd;
    TextView txtDescription;
    TextView txtAddress;
    TextView txtPhone;

    View layout;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        layout = inflater.inflate(R.layout.tab_add_office, container, false);

        txtDescription = layout.findViewById(R.id.txtDescription);
        txtAddress = layout.findViewById(R.id.txtAddress);
        txtPhone = layout.findViewById(R.id.txtPhone);

        btnAdd = layout.findViewById(R.id.btnAddOffice);
        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveOffice();
            }
        });

        return layout;
    }

    private void saveOffice() {
        DOffice dOffice = new DOffice(getContext());
        Office office = new Office();

        office.setDescription(txtDescription.getText().toString());
        office.setAddress(txtAddress.getText().toString());
        office.setPhone(txtPhone.getText().toString());

        String msg;

        if (dOffice.insertOffice(office) > 0)
            msg = "Se registro el consultorio";
        else
            msg = "Error al registrar el consultorio";

        Snackbar.make(layout, msg, Snackbar.LENGTH_LONG).show();
    }
}
