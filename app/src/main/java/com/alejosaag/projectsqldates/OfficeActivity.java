package com.alejosaag.projectsqldates;

import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTabHost;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class OfficeActivity extends FragmentActivity {
    private FragmentTabHost tabHost;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_office);

        tabHost = (FragmentTabHost) findViewById(R.id.tabHostOffice);
        tabHost.setup(this, getSupportFragmentManager(), R.id.tabContentOffice);
        tabHost.addTab(tabHost.newTabSpec("tab1").setIndicator("Listado"), TabListOffices.class, null);
        tabHost.addTab(tabHost.newTabSpec("tab2").setIndicator("Agregar"), TabAddOffice.class, null);
    }
}
