package com.alejosaag.projectsqldates.Data;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class ConexionBD extends SQLiteOpenHelper {
    // Version de la base de datos.
    public static final int DATABASE_VERSION = 1;
    // Nombre de la base de datos.
    private static final String DATABASE_NAME = "bd_citas";
    // Instancia de la clase SQLiteDatabase.
    protected SQLiteDatabase db;

    public ConexionBD(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String sql = "";

        // Se definen las tablas sin llaves foraneas.
        sql += "CREATE TABLE especialidades (";
        sql += "id INTEGER PRIMARY KEY AUTOINCREMENT, ";
        sql += "descripcion TEXT";
        sql += ");";
        db.execSQL(sql);

        sql = "CREATE TABLE consultorios (";
        sql += "id INTEGER PRIMARY KEY AUTOINCREMENT, ";
        sql += "descripcion TEXT, ";
        sql += "direccion TEXT, ";
        sql += "telefono TEXT";
        sql += ");";
        db.execSQL(sql);

        sql = "CREATE TABLE pacientes (";
        sql += "id INTEGER PRIMARY KEY AUTOINCREMENT, ";
        sql += "identificacion TEXT UNIQUE, ";
        sql += "nombres TEXT, ";
        sql += "apellidos TEXT";
        sql += ");";
        db.execSQL(sql);

        sql = "CREATE TABLE medicos (";
        sql += "id INTEGER PRIMARY KEY AUTOINCREMENT, ";
        sql += "identificacion TEXT UNIQUE, ";
        sql += "nombres TEXT, ";
        sql += "apellidos TEXT, ";
        sql += "especialidad_id INTEGER, ";
        sql += "FOREIGN KEY (especialidad_id) REFERENCES especialidades(id)";
        sql += ");";
        db.execSQL(sql);

        sql = "CREATE TABLE citas (";
        sql += "id INTEGER PRIMARY KEY AUTOINCREMENT, ";
        sql += "paciente_id INTEGER, ";
        sql += "medico_id INTEGER, ";
        sql += "consultorio_id INTEGER, ";
        sql += "fecha TEXT, ";
        sql += "hora TEXT, ";
        sql += "FOREIGN KEY (paciente_id) REFERENCES paciente(id), ";
        sql += "FOREIGN KEY (medico_id) REFERENCES medicos(id), ";
        sql += "FOREIGN KEY (consultorio_id) REFERENCES consultorios(id)";
        sql += ");";
        db.execSQL(sql);

        // Insercion de registros iniciales.
        sql = "INSERT INTO especialidades(descripcion) VALUES ";
        sql += "('Medico general'), ";
        sql += "('Odontologia'), ";
        sql += "('Ginecologo'), ";
        sql += "('Fisioterapeuta');";
        db.execSQL(sql);

        sql = "INSERT INTO consultorios(descripcion, direccion, telefono) VALUES ";
        sql += "('Consultorio 1', 'Calle 13 # 100-120', '1234567'), ";
        sql += "('Consultorio 2', 'Calle 5 # 50-60', '3456789'), ";
        sql += "('Consultorio 3', 'Calle 9 # 50-12', '5678901'), ";
        sql += "('Consultorio 4', 'Calle 5 # 70-33', '7890123');";
        db.execSQL(sql);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        String sql = "";
        sql = "DROP TABLE IF EXISTS citas;";
        db.execSQL(sql);

        sql = "DROP TABLE IF EXISTS medicos;";
        db.execSQL(sql);

        sql = "DROP TABLE IF EXISTS especialidaes;";
        db.execSQL(sql);

        sql = "DROP TABLE IF EXISTS consultorios;";
        db.execSQL(sql);

        sql = "DROP TABLE IF EXISTS paceintes;";
        db.execSQL(sql);

        onCreate(db);
    }

    private boolean insertarSql(String sql) {
        this.db = this.getWritableDatabase();
        db.execSQL(sql);
        db.close();
        return true;
    }

    protected long insertarRegistro(ContentValues values, String table) {
        SQLiteDatabase db = this.getWritableDatabase();
        long res = db.insert(table, null, values);
        db.close();

        return res;
    }

    public int getHighestId() {
        final String MY_QUERY = "SELECT last_insert_rowid()";
        Cursor cur = db.rawQuery(MY_QUERY, null);
        cur.moveToFirst();
        int id = cur.getInt(0);
        cur.close();
        db.close();

        return id;
    }
}
