package com.alejosaag.projectsqldates;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import com.alejosaag.projectsqldates.Adapters.OfficeAdapter;
import com.alejosaag.projectsqldates.Entities.Office;
import com.alejosaag.projectsqldates.Models.DOffice;

import java.lang.reflect.Array;
import java.util.ArrayList;

public class TabListOffices extends Fragment {
    ListView listOffices;
    View layout = null;
    Button btnSearch;
    EditText txtDescription;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        layout = inflater.inflate(R.layout.tab_list_offices, container, false);
        listOffices = layout.findViewById(R.id.lstOffices);
        btnSearch = layout.findViewById(R.id.btnSearchOffice);
        txtDescription = layout.findViewById(R.id.txtDescription);

        loadOfficesList();

        btnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchOffice();
            }
        });
        listOffices.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Office office = (Office) listOffices.getItemAtPosition(position);

                Intent intent = new Intent(getContext(), EditOffice.class);
                intent.putExtra("selectedOffice", office);
                startActivity(intent);
            }
        });

        return layout;
    }

    private void searchOffice() {
        DOffice dOffice = new DOffice(getContext());

        String description = txtDescription.getText().toString();

        if (description.equals("")) {
            Snackbar.make(layout, "Por favor ingrese una descripción", Snackbar.LENGTH_LONG).show();

            return;
        }

        Office office = new Office();

        office = dOffice.getOfficeByDescription(description);

        if (office != null) {
            ArrayList<Office> list = new ArrayList<>();

            list.add(office);

            OfficeAdapter adapter = new OfficeAdapter(getContext(), list);

            listOffices.setAdapter(adapter);
        } else {
            Snackbar.make(layout, "No se encontro el consultorio que buscas", Snackbar.LENGTH_LONG).show();
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        loadOfficesList();
    }

    private void loadOfficesList() {
        DOffice dOffice = new DOffice(getContext());

        ArrayList<Office> listOffice = new ArrayList<>();
        listOffice = dOffice.getOffices();

        OfficeAdapter adapter = new OfficeAdapter(getContext(), listOffice);

        listOffices.setAdapter(adapter);
    }
}
