package com.alejosaag.projectsqldates;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.alejosaag.projectsqldates.Entities.Office;
import com.alejosaag.projectsqldates.Models.DOffice;

public class EditOffice extends AppCompatActivity {
    EditText txtDescripton;
    EditText txtAddress;
    EditText txtPhone;
    Button btnEdit;
    Button btnDelete;
    Button btnMainMenu;

    Office selectedOffice;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_office);

        selectedOffice = (Office)getIntent().getSerializableExtra("selectedOffice");

        txtDescripton = findViewById(R.id.txtDescription);
        txtAddress = findViewById(R.id.txtAddress);
        txtPhone = findViewById(R.id.txtPhone);

        txtDescripton.setText(selectedOffice.getDescription());
        txtAddress.setText(selectedOffice.getAddress());
        txtPhone.setText(selectedOffice.getPhone());

        btnEdit = findViewById(R.id.btnEdit);
        btnEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editOffice();
            }
        });
        btnDelete = findViewById(R.id.btnDelete);
        btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteOffice();
            }
        });
        btnMainMenu = findViewById(R.id.btnMainMenu);
        btnMainMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToMainMenu();
            }
        });
    }

    private void goToMainMenu() {
        Intent intent = new Intent(getBaseContext(), MainActivity.class);
        startActivity(intent);
    }

    private void deleteOffice() {
        DOffice dOffice = new DOffice(getBaseContext());

        int idSelectedOffice = selectedOffice.getId();

        String msg;
        if (dOffice.deleteOffice(idSelectedOffice) > 0) {
            Toast.makeText(this, "Consultorio eliminado correctamente", Toast.LENGTH_LONG).show();
            finish();
        } else {
            Toast.makeText(this, "Error al eliminar el consultorio", Toast.LENGTH_LONG).show();
        }
    }

    private void editOffice() {
        int idSelectedOffice = selectedOffice.getId();

        Office office = new Office();

        office.setId(idSelectedOffice);
        office.setDescription(txtDescripton.getText().toString());
        office.setAddress(txtAddress.getText().toString());
        office.setPhone(txtPhone.getText().toString());

        DOffice dOffice = new DOffice(getBaseContext());

        if (dOffice.updateOffice(office) > 0) {
            Toast.makeText(this, "Consultorio actualizado", Toast.LENGTH_SHORT).show();
            finish();
        } else {
            Toast.makeText(this, "Error al actualizar el consultorio", Toast.LENGTH_LONG).show();
        }
    }
}
