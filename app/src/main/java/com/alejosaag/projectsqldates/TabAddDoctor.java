package com.alejosaag.projectsqldates;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.alejosaag.projectsqldates.Entities.Doctor;
import com.alejosaag.projectsqldates.Entities.Speciality;
import com.alejosaag.projectsqldates.Models.DDoctor;
import com.alejosaag.projectsqldates.Models.DSpeciality;

import java.util.ArrayList;

public class TabAddDoctor extends Fragment {
    Button btnAdd;
    EditText txtIdentification;
    EditText txtNames;
    EditText txtLastName;
    Spinner cboSpeciallity;
    View layout;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        layout = inflater.inflate(R.layout.tab_add_doctor, container, false);

        txtIdentification = layout.findViewById(R.id.txtIdentification);
        txtNames = layout.findViewById(R.id.txtName);
        txtLastName = layout.findViewById(R.id.txtLastName);
        cboSpeciallity = layout.findViewById(R.id.cboSpeciallity);

        this.loadSpinnerSpeciallities();

        btnAdd = layout.findViewById(R.id.btnAdd);
        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveDoctor();
            }
        });

        return layout;
    }

    private void saveDoctor() {
        DDoctor dDoctor=new DDoctor(getContext());
        Doctor doctor= new Doctor();

        doctor.setIdentificacion(txtIdentification.getText().toString());
        doctor.setNombres(txtNames.getText().toString());
        doctor.setApellidos(txtLastName.getText().toString());
        doctor.setEspecialidad_id(((Speciality)cboSpeciallity.getSelectedItem()).getId());

        if (dDoctor.insertDoctor(doctor) > 0)
            Toast.makeText(getContext(), "Registro insertado", Toast.LENGTH_SHORT).show();
        else
            Toast.makeText(getContext(), "Registro no insertado", Toast.LENGTH_SHORT).show();
    }

    private void loadSpinnerSpeciallities() {
        DSpeciality dSpeciality = new DSpeciality(getContext());

        ArrayList<Speciality> lstSpeciallyties = new ArrayList<Speciality>();
        lstSpeciallyties = dSpeciality.getSpecialities();

        ArrayAdapter<Speciality> adapter = new ArrayAdapter<Speciality>(getContext(), android.R.layout.simple_spinner_dropdown_item, lstSpeciallyties);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        cboSpeciallity.setAdapter(adapter);
    }
}
