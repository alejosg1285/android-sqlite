package com.alejosaag.projectsqldates;

import android.content.Intent;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.alejosaag.projectsqldates.Entities.Appointment;
import com.alejosaag.projectsqldates.Entities.Doctor;
import com.alejosaag.projectsqldates.Entities.Office;
import com.alejosaag.projectsqldates.Entities.Patient;
import com.alejosaag.projectsqldates.Entities.Speciality;
import com.alejosaag.projectsqldates.Models.DAppointment;
import com.alejosaag.projectsqldates.Models.DDoctor;
import com.alejosaag.projectsqldates.Models.DOffice;
import com.alejosaag.projectsqldates.Models.DPatient;

import java.util.ArrayList;

public class EditAppointment extends AppCompatActivity {
    Spinner cboPatient;
    Spinner cboDoctor;
    Spinner cboOffice;
    EditText txtDate;
    EditText txtHour;
    Button btnEdit;
    Button btnDelete;
    Button btnMainMenu;

    Appointment appointmentSelected;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_appointment);

        appointmentSelected = (Appointment)getIntent().getSerializableExtra("appointmentSelected");

        cboDoctor = findViewById(R.id.cboDoctor);
        cboOffice = findViewById(R.id.cboOffice);
        cboPatient = findViewById(R.id.cboPatient);
        txtDate = findViewById(R.id.txtDate);
        txtHour = findViewById(R.id.txtHour);
        btnEdit = findViewById(R.id.btnEdit);
        btnDelete = findViewById(R.id.btnDelete);
        btnMainMenu = findViewById(R.id.btnMainMenu);

        this.loadSpinnerDoctors();
        this.loadSpinnerPatients();
        this.loadSpinnerOffices();

        cboDoctor.setSelection(getIndexDoctor(cboDoctor, appointmentSelected.getDoctorId()));
        cboOffice.setSelection(getIndexOffice(cboOffice, appointmentSelected.getOfficeId()));
        cboPatient.setSelection(getIndexPatient(cboPatient, appointmentSelected.getPatientId()));
        txtDate.setText(appointmentSelected.getDate());
        txtHour.setText(appointmentSelected.getHour());

        btnEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editAppointment();
            }
        });
        btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteAppointment();
            }
        });
        btnMainMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToMainMenu();
            }
        });
    }

    private int getIndexOffice(Spinner spinner, int id) {
        int index = 0;

        for (int i = 0; i < spinner.getCount(); i++) {
            if (((Office) spinner.getItemAtPosition(i)).getId() == id) {
                index = i;
                break;
            }
        }

        return index;
    }

    private int getIndexDoctor(Spinner cboDoctor, int doctorId) {
        int index = 0;

        for (int i = 0; i < cboDoctor.getCount(); i++) {
            if (((Doctor) cboDoctor.getItemAtPosition(i)).getId() == doctorId) {
                index = i;
                break;
            }
        }

        return index;
    }

    private void goToMainMenu() {
        Intent intent = new Intent(getBaseContext(), MainActivity.class);
        startActivity(intent);
    }

    private void deleteAppointment() {
        int idSelected = appointmentSelected.getId();

        DAppointment dAppointment = new DAppointment(getBaseContext());

        if (dAppointment.deleteAppointment(idSelected) > 0) {
            Snackbar.make(getCurrentFocus(), "Se ha eliminado la cita", Snackbar.LENGTH_LONG).show();
            finish();
        } else {
            Snackbar.make(getCurrentFocus(),"Error al eliminar la cita", Snackbar.LENGTH_LONG).show();
        }
    }

    private void editAppointment() {
        int idSelected = appointmentSelected.getId();

        Appointment appointment = new Appointment();

        appointment.setId(idSelected);
        appointment.setDoctorId(((Doctor)cboDoctor.getSelectedItem()).getId());
        appointment.setPatientId(((Patient)cboPatient.getSelectedItem()).getId());
        appointment.setOfficeId(((Office)cboOffice.getSelectedItem()).getId());
        appointment.setDate(txtDate.getText().toString());
        appointment.setHour(txtHour.getText().toString());

        DAppointment dAppointment = new DAppointment(getBaseContext());

        if (dAppointment.updateAppointment(appointment) > 0) {
            Toast.makeText(this, "Registro actualizado", Toast.LENGTH_LONG).show();
            finish();
        } else {
            Toast.makeText(this, "Error al actualizar la cita", Toast.LENGTH_LONG).show();
        }
    }

    private void loadSpinnerOffices() {
        DOffice dOffice = new DOffice(getBaseContext());

        ArrayList<Office> listOffices = new ArrayList<>();
        listOffices = dOffice.getOffices();

        ArrayAdapter<Office> adapter = new ArrayAdapter<>(getBaseContext(), android.R.layout.simple_spinner_dropdown_item, listOffices);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        cboOffice.setAdapter(adapter);
    }

    private void loadSpinnerPatients() {
        DPatient dPatient = new DPatient(getBaseContext());

        ArrayList<Patient> listPatients = new ArrayList<>();
        listPatients = dPatient.getPatients();

        ArrayAdapter<Patient> adapter = new ArrayAdapter<>(getBaseContext(), android.R.layout.simple_spinner_dropdown_item, listPatients);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        cboPatient.setAdapter(adapter);
    }

    private void loadSpinnerDoctors() {
        DDoctor dDoctor = new DDoctor(getBaseContext());

        ArrayList<Doctor> listDoctors = new ArrayList<>();
        listDoctors = dDoctor.getDoctors();

        ArrayAdapter<Doctor> adapter = new ArrayAdapter<>(getBaseContext(), android.R.layout.simple_spinner_dropdown_item, listDoctors);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        cboDoctor.setAdapter(adapter);
    }

    private int getIndexPatient(Spinner spinner, int id) {
        int index = 0;

        for (int i = 0; i < spinner.getCount(); i++) {
            if (((Patient) spinner.getItemAtPosition(i)).getId() == id) {
                index = i;
                break;
            }
        }

        return index;
    }
}
