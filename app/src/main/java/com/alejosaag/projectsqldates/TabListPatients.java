package com.alejosaag.projectsqldates;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.alejosaag.projectsqldates.Adapters.PatientAdapter;
import com.alejosaag.projectsqldates.Entities.Patient;
import com.alejosaag.projectsqldates.Models.DPatient;

import java.util.ArrayList;

public class TabListPatients extends Fragment {
    ListView listPatients;
    View layout;
    Button btnSearch;
    EditText txtIdentification;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        layout = inflater.inflate(R.layout.tab_list_patients, container, false);
        listPatients = layout.findViewById(R.id.lstPatients);
        btnSearch = layout.findViewById(R.id.btnSearch);
        txtIdentification = layout.findViewById(R.id.txtIdentificationPatient);

        loadPatientsList();

        btnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchPatient();
            }
        });

        listPatients.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Patient patient = (Patient)listPatients.getItemAtPosition(position);

                Intent intent = new Intent(getContext(), EditPatient.class);
                intent.putExtra("selectedPatient", patient);
                startActivity(intent);
            }
        });

        return layout;
    }

    @Override
    public void onResume() {
        super.onResume();

        loadPatientsList();
    }

    private void loadPatientsList() {
        DPatient dPatient = new DPatient(getContext());

        ArrayList<Patient> lstPatients = new ArrayList<>();
        lstPatients = dPatient.getPatients();

        PatientAdapter adapter = new PatientAdapter(getContext(), lstPatients);

        listPatients.setAdapter(adapter);
    }

    private void searchPatient() {
        DPatient dPatient = new DPatient(getContext());

        String identification = txtIdentification.getText().toString();

        if (identification.trim().equals("")) {
            Toast.makeText(getContext(), "Debe ingresar un numero de identificación", Toast.LENGTH_LONG).show();

            return;
        }

        Patient patient = dPatient.getPatientByIdentification(identification);

        if (patient != null) {
            ArrayList<Patient> lstPatients = new ArrayList<>();

            lstPatients.add(patient);

            PatientAdapter adapter = new PatientAdapter(getContext(), lstPatients);

            listPatients.setAdapter(adapter);
        } else {
            Toast.makeText(getContext(), "No se encontro el paciente que buscas", Toast.LENGTH_LONG).show();
        }
    }
}
