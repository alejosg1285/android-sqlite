package com.alejosaag.projectsqldates.Models;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.alejosaag.projectsqldates.Data.ConexionBD;
import com.alejosaag.projectsqldates.Entities.Patient;

import java.util.ArrayList;

public class DPatient extends ConexionBD {
    private String table = "pacientes";

    public DPatient(Context context) {
        super(context);
    }

    public long insertPatient(Patient patient) {
        ContentValues values = new ContentValues();
        values.put("identificacion", patient.getIdentificacion());
        values.put("nombres", patient.getNombres());
        values.put("apellidos", patient.getApellidos());

        return this.insertarRegistro(values, table);
    }

    public Patient getPatientById(int id) {
        Patient patient = null;

        String sql = String.format("SELECT id, identificacion, nombres, apellidos FROM %s WHERE id = %d", table, id);

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor c = null;

        c = db.rawQuery(sql, null);

        if (c.moveToFirst()) {
            patient = new Patient();
            patient.setId(c.getInt(0));
            patient.setIdentificacion(c.getString(1));
            patient.setNombres(c.getString(2));
            patient.setApellidos(c.getString(3));
        }

        db.close();

        return patient;
    }

    public Patient getPatientByIdentification(String identification) {
        Patient patient = null;

        String sql = String.format("SELECT id, identificacion, nombres, apellidos FROM %s WHERE identificacion = %s", table, identification);

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor c = null;

        c = db.rawQuery(sql, null);

        if (c.moveToFirst()) {
            patient = new Patient();
            patient.setId(c.getInt(0));
            patient.setIdentificacion(c.getString(1));
            patient.setNombres(c.getString(2));
            patient.setApellidos(c.getString(3));
        }

        db.close();

        return patient;
    }

    public ArrayList<Patient> getPatients() {
        ArrayList<Patient> lstPatients = new ArrayList<>();

        String sql = String.format("SELECT id, identificacion, nombres, apellidos FROM %s", table);

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor c = db.rawQuery(sql, null);

        if (c.moveToFirst()) {
            do {
                Patient patient = new Patient();
                patient.setId(c.getInt(0));
                patient.setIdentificacion(c.getString(1));
                patient.setNombres(c.getString(2));
                patient.setApellidos(c.getString(3));

                lstPatients.add(patient);
            } while (c.moveToNext());
        }

        db.close();

        return lstPatients;
    }

    public int updatePatient(Patient patient) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put("identificacion", patient.getIdentificacion());
        values.put("nombres", patient.getNombres());
        values.put("apellidos", patient.getApellidos());

        int res = db.update(table, values, String.format("id = %d", patient.getId()), null);
        db.close();

        return res;
    }

    public int deletePatient(int id) {
        SQLiteDatabase db = this.getWritableDatabase();

        int res = db.delete(table, String.format("id = %d", id), null);

        db.close();

        return res;
    }
}
