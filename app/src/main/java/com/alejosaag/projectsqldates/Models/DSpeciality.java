package com.alejosaag.projectsqldates.Models;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.alejosaag.projectsqldates.Data.ConexionBD;
import com.alejosaag.projectsqldates.Entities.Speciality;

import java.util.ArrayList;

public class DSpeciality extends ConexionBD {
    private String table = "especialidades";

    public DSpeciality(Context context) {
        super(context);
    }

    public ArrayList<Speciality> getSpecialities() {
        ArrayList<Speciality> lstSpecialities = new ArrayList<Speciality>();

        String sql = "SELECT id, descripcion from especialidades";

        SQLiteDatabase db = this.getWritableDatabase();

        Cursor c = null;

        c = db.rawQuery(sql, null);

        if (c.moveToFirst()) {
            do {
                Speciality speciality = new Speciality();
                speciality.setId(c.getInt(0));
                speciality.setDescripcion(c.getString(1));

                lstSpecialities.add(speciality);
            } while (c.moveToNext());
        }

        db.close();

        return lstSpecialities;
    }
}
