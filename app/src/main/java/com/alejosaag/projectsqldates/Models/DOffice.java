package com.alejosaag.projectsqldates.Models;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.alejosaag.projectsqldates.Data.ConexionBD;
import com.alejosaag.projectsqldates.Entities.Office;

import java.util.ArrayList;

public class DOffice extends ConexionBD {
    private String table = "consultorios";

    public DOffice(Context context) {
        super(context);
    }

    public long insertOffice(Office office) {
        ContentValues values = new ContentValues();
        values.put("descripcion", office.getDescription());
        values.put("direccion", office.getAddress());
        values.put("telefono", office.getPhone());

        return this.insertarRegistro(values, table);
    }

    public Office getOfficeById(int id) {
        Office office = null;

        String sql = String.format("SELECT id, descripcion, direccion, telefono from %s WHERE id = %d", table, id);

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor c = db.rawQuery(sql, null);

        if (c.moveToFirst()) {
            office = new Office();
            office.setId(c.getInt(0));
            office.setDescription(c.getString(1));
            office.setAddress(c.getString(2));
            office.setPhone(c.getString(3));
        }

        db.close();

        return office;
    }

    public Office getOfficeByDescription(String desc) {
        Office office = null;

        String sql = String.format("SELECT id, descripcion, direccion, telefono from %s WHERE descripcion LIKE %%s%", table, desc);

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor c = db.rawQuery(sql, null);

        if (c.moveToFirst()) {
            office = new Office();
            office.setId(c.getInt(0));
            office.setDescription(c.getString(1));
            office.setAddress(c.getString(2));
            office.setPhone(c.getString(3));
        }

        db.close();

        return office;
    }

    public ArrayList<Office> getOffices() {
        ArrayList<Office> listOffices = new ArrayList<>();

        String sql = String.format("SELECT id, descripcion, direccion, telefono from %s", table);

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor c = db.rawQuery(sql, null);

        if (c.moveToFirst()) {
            do {
                Office office = new Office();
                office.setId(c.getInt(0));
                office.setDescription(c.getString(1));
                office.setAddress(c.getString(2));
                office.setPhone(c.getString(3));

                listOffices.add(office);
            } while (c.moveToNext());
        }

        db.close();

        return listOffices;
    }

    public int updateOffice(Office office) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put("descripcion", office.getDescription());
        values.put("direccion", office.getAddress());
        values.put("telefono", office.getPhone());

        int res = db.update(table, values, String.format("id = %d", office.getId()), null);
        db.close();

        return res;
    }

    public int deleteOffice(int id) {
        SQLiteDatabase db = this.getWritableDatabase();

        int res = db.delete(table, String.format("id = %d", id), null);

        db.close();

        return res;
    }
}
