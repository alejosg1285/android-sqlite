package com.alejosaag.projectsqldates.Models;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.alejosaag.projectsqldates.Data.ConexionBD;
import com.alejosaag.projectsqldates.Entities.Doctor;

import java.util.ArrayList;

public class DDoctor extends ConexionBD {
    private String table = "medicos";

    public DDoctor(Context context) {
        super(context);
    }

    public long insertDoctor(Doctor doctor) {
        ContentValues values = new ContentValues();
        values.put("identificacion", doctor.getIdentificacion());
        values.put("nombres", doctor.getNombres());
        values.put("apellidos", doctor.getApellidos());
        values.put("especialidad_id", doctor.getEspecialidad_id());

        return this.insertarRegistro(values, table);
    }

    public Doctor getDoctorById(int id) {
        Doctor doctor = null;

        String sql = "SELECT m.id, m.identificacion, m.nombres, m.apellidos, e.id, e.descripcion ";
        sql += "FROM medicos m, especialidades e WHERE m.especialidad_id = e.id ";
        sql += "AND m.id = " + id;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor c = null;

        c = db.rawQuery(sql, null);

        if (c.moveToFirst()) {
            doctor = new Doctor();
            doctor.setId(c.getInt(0));
            doctor.setIdentificacion(c.getString(1));
            doctor.setNombres(c.getString(2));
            doctor.setApellidos(c.getString(3));
            doctor.setEspecialidad_id(c.getInt(4));
            doctor.setDescEsp(c.getString(5));
        }

        db.close();

        return doctor;
    }

    public Doctor getDoctorByIdentification(String identification) {
        Doctor doctor = null;

        String sql = "SELECT m.id, m.identificacion, m.nombres, m.apellidos, e.id, e.descripcion ";
        sql += "FROM medicos m, especialidades e WHERE m.especialidad_id = e.id ";
        sql += "AND m.identificacion = " + identification;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor c = null;

        c = db.rawQuery(sql, null);

        if (c.moveToFirst()) {
            doctor = new Doctor();
            doctor.setId(c.getInt(0));
            doctor.setIdentificacion(c.getString(1));
            doctor.setNombres(c.getString(2));
            doctor.setApellidos(c.getString(3));
            doctor.setEspecialidad_id(c.getInt(4));
            doctor.setDescEsp(c.getString(5));
        }

        db.close();

        return doctor;
    }

    public ArrayList<Doctor> getDoctors() {
        ArrayList<Doctor> lstDoctors = new ArrayList<Doctor>();

        String sql = "SELECT m.id, m.identificacion, m.nombres, m.apellidos, e.id, e.descripcion ";
        sql += "FROM medicos m, especialidades e WHERE m.especialidad_id = e.id";

        SQLiteDatabase db = this.getWritableDatabase();

        Cursor c = null;
        c = db.rawQuery(sql, null);

        if (c.moveToFirst()) {
            do {
                Doctor doctor = new Doctor();
                doctor.setId(c.getInt(0));
                doctor.setIdentificacion(c.getString(1));
                doctor.setNombres(c.getString(2));
                doctor.setApellidos(c.getString(3));
                doctor.setEspecialidad_id(c.getInt(4));
                doctor.setDescEsp(c.getString(5));

                lstDoctors.add(doctor);
            } while (c.moveToNext());
        }

        db.close();

        return lstDoctors;
    }

    public int updateDoctor(Doctor doctor) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put("identificacion", doctor.getIdentificacion());
        values.put("nombres", doctor.getNombres());
        values.put("apellidos", doctor.getApellidos());
        values.put("especialidad_id", doctor.getEspecialidad_id());

        int res = db.update(this.table, values, "id = " + doctor.getId(), null);
        db.close();

        return res;
    }

    public int deleteDoctor(int id) {
        SQLiteDatabase db = this.getWritableDatabase();

        int res = db.delete(this.table, "id = " + id, null);

        db.close();

        return res;
    }
}
