package com.alejosaag.projectsqldates.Models;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.alejosaag.projectsqldates.Data.ConexionBD;
import com.alejosaag.projectsqldates.Entities.Appointment;

import java.util.ArrayList;

public class DAppointment extends ConexionBD {
    private String table ="citas";

    public DAppointment(Context context) {
        super(context);
    }

    public long insertAppointment(Appointment appointment) {
        ContentValues values = new ContentValues();
        values.put("paciente_id", appointment.getPatientId());
        values.put("medico_id", appointment.getDoctorId());
        values.put("consultorio_id", appointment.getOfficeId());
        values.put("fecha", appointment.getDate());
        values.put("hora", appointment.getHour());

        return this.insertarRegistro(values, table);
    }

    public ArrayList<Appointment> getAppointmentByDoctorName(String doctorName) {
        ArrayList<Appointment> listAppointments = new ArrayList<>();

        String sql = "SELECT c.id, m.id, m.nombres, m.apellidos, p.id, p.nombres, p.apellidos, o.id, o.descripcion, c.fecha, c.hora ";
        sql += "FROM citas c, medicos m, pacientes p, consultorios o ";
        sql += "WHERE c.paciente_id = p.id AND c.medico_id = m.id AND c.consultorio_id = o.id ";
        sql += String.format("AND m.nombres LIKE %%s% ", doctorName);
        sql += String.format("OR m.apellidos LIKE %%s%", doctorName);

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor c = db.rawQuery(sql, null);

        if (c.moveToFirst()) {
            do {
                Appointment appointment = new Appointment();
                appointment.setId(c.getInt(0));
                appointment.setDoctorId(c.getInt(1));
                appointment.setDoctorName(String.format("%s %s", c.getString(2), c.getString(3)));
                appointment.setPatientId(c.getInt(4));
                appointment.setPatientName(String.format("%s %s", c.getString(5), c.getString(6)));
                appointment.setOfficeId(c.getInt(7));
                appointment.setOfficeDescription(c.getString(8));
                appointment.setDate(c.getString(9));
                appointment.setHour(c.getString(10));

                listAppointments.add(appointment);
            } while (c.moveToNext());
        }

        db.close();

        return listAppointments;
    }

    public ArrayList<Appointment> getAppointmentByPatientIdentification(String identification) {
        ArrayList<Appointment> listAppointments = new ArrayList<>();

        String sql = "SELECT c.id, m.id, m.nombres, m.apellidos, p.id, p.nombres, p.apellidos, o.id, o.descripcion, c.fecha, c.hora ";
        sql += "FROM citas c, medicos m, pacientes p, consultorios o ";
        sql += "WHERE c.paciente_id = p.id AND c.medico_id = m.id AND c.consultorio_id = o.id ";
        sql += String.format("AND p.identificacion = %s", identification);

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor c = db.rawQuery(sql, null);

        if (c.moveToFirst()) {
            do {
                Appointment appointment = new Appointment();
                appointment.setId(c.getInt(0));
                appointment.setDoctorId(c.getInt(1));
                appointment.setDoctorName(String.format("%s %s", c.getString(2), c.getString(3)));
                appointment.setPatientId(c.getInt(4));
                appointment.setPatientName(String.format("%s %s", c.getString(5), c.getString(6)));
                appointment.setOfficeId(c.getInt(7));
                appointment.setOfficeDescription(c.getString(8));
                appointment.setDate(c.getString(9));
                appointment.setHour(c.getString(10));

                listAppointments.add(appointment);
            } while (c.moveToNext());
        }

        db.close();

        return listAppointments;
    }

    public ArrayList<Appointment> getAppointments() {
        ArrayList<Appointment> listAppointments = new ArrayList<>();

        String sql = "SELECT c.id, m.id, m.nombres, m.apellidos, p.id, p.nombres, p.apellidos, o.id, o.descripcion, c.fecha, c.hora ";
        sql += "FROM citas c, medicos m, pacientes p, consultorios o ";
        sql += "WHERE c.paciente_id = p.id AND c.medico_id = m.id AND c.consultorio_id = o.id ";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor c = db.rawQuery(sql, null);

        if (c.moveToFirst()) {
            do {
                Appointment appointment = new Appointment();
                appointment.setId(c.getInt(0));
                appointment.setDoctorId(c.getInt(1));
                appointment.setDoctorName(String.format("%s %s", c.getString(2), c.getString(3)));
                appointment.setPatientId(c.getInt(4));
                appointment.setPatientName(String.format("%s %s", c.getString(5), c.getString(6)));
                appointment.setOfficeId(c.getInt(7));
                appointment.setOfficeDescription(c.getString(8));
                appointment.setDate(c.getString(9));
                appointment.setHour(c.getString(10));

                listAppointments.add(appointment);
            } while (c.moveToNext());
        }

        db.close();

        return listAppointments;
    }

    public int updateAppointment(Appointment appointment) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put("paciente_id", appointment.getPatientId());
        values.put("medico_id", appointment.getDoctorId());
        values.put("consultorio_id", appointment.getOfficeId());
        values.put("fecha", appointment.getDate());
        values.put("hora", appointment.getHour());

        int res = db.update(table, values, String.format("id = %d", appointment.getId()), null);
        db.close();

        return res;
    }

    public int deleteAppointment(int id) {
        SQLiteDatabase db = this.getWritableDatabase();

        int res = db.delete(table, String.format("id = %d", id), null);
        db.close();

        return res;
    }
}
