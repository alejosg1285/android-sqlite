package com.alejosaag.projectsqldates.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.alejosaag.projectsqldates.Entities.Doctor;
import com.alejosaag.projectsqldates.R;

import java.util.ArrayList;

public class DoctorAdapter extends BaseAdapter {
    private ArrayList<Doctor> listData;
    private LayoutInflater layoutInflater;
    Context context;
    LayoutInflater inflater;

    public DoctorAdapter(Context context, ArrayList<Doctor> listData) {
        this.listData = listData;
        layoutInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return listData.size();
    }

    @Override
    public Object getItem(int position) {
        return listData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = layoutInflater.inflate(R.layout.items_listview_doctors, null);

        TextView txtId = convertView.findViewById(R.id.lblId);
        txtId.setText(String.valueOf((listData.get(position)).getId()));

        TextView txtIdentification = convertView.findViewById(R.id.lblIdentification);
        txtIdentification.setText((listData.get(position)).getIdentificacion());

        String fullName = String.format("%s %s", (listData.get(position)).getNombres(), (listData.get(position)).getApellidos());
        TextView txtFullName = convertView.findViewById(R.id.lblFullName);
        txtFullName.setText(fullName);

        TextView txtSpeciallity = convertView.findViewById(R.id.lblSpeciallity);
        txtSpeciallity.setText((listData.get(position)).getDescEsp());

        return convertView;
    }
}
