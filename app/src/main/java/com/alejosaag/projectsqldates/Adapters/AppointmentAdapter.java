package com.alejosaag.projectsqldates.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.alejosaag.projectsqldates.Entities.Appointment;
import com.alejosaag.projectsqldates.R;

import java.util.ArrayList;

public class AppointmentAdapter extends BaseAdapter {
    private ArrayList<Appointment> listAppointments;
    private LayoutInflater layoutInflater;
    Context context;
    LayoutInflater inflater;

    public AppointmentAdapter(ArrayList<Appointment> listAppointments, Context context) {
        this.listAppointments = listAppointments;
        this.layoutInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return listAppointments.size();
    }

    @Override
    public Object getItem(int position) {
        return listAppointments.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = layoutInflater.inflate(R.layout.items_listview_appointments, null);

        TextView lblId = convertView.findViewById(R.id.lblId);
        lblId.setText(String.valueOf((listAppointments.get(position)).getId()));

        TextView lblPatientName = convertView.findViewById(R.id.lblPatientName);
        lblPatientName.setText((listAppointments.get(position)).getPatientName());

        TextView doctorName = convertView.findViewById(R.id.lblDoctorName);
        doctorName.setText((listAppointments.get(position)).getDoctorName());

        TextView txtOffice = convertView.findViewById(R.id.lblOfficeName);
        txtOffice.setText((listAppointments.get(position)).getOfficeDescription());

        TextView txtDate = convertView.findViewById(R.id.lblDate);
        txtDate.setText((listAppointments.get(position)).getDate());

        TextView txtHour = convertView.findViewById(R.id.lbHour);
        txtHour.setText((listAppointments.get(position)).getHour());

        return convertView;
    }
}
