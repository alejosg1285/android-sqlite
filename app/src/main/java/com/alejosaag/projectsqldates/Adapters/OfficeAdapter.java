package com.alejosaag.projectsqldates.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.alejosaag.projectsqldates.Entities.Office;
import com.alejosaag.projectsqldates.R;

import java.util.ArrayList;

public class OfficeAdapter extends BaseAdapter {
    private ArrayList<Office> listOffice;
    private LayoutInflater layoutInflater;
    Context context;
    LayoutInflater inflater;

    public OfficeAdapter(Context context, ArrayList<Office> listOffice) {
        this.listOffice = listOffice;
        layoutInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return listOffice.size();
    }

    @Override
    public Object getItem(int position) {
        return listOffice.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = layoutInflater.inflate(R.layout.items_listview_offices, null);

        TextView txtId = convertView.findViewById(R.id.lblId);
        txtId.setText(String.valueOf((listOffice.get(position)).getId()));

        TextView txtDescription = convertView.findViewById(R.id.lblDescription);
        txtDescription.setText((listOffice.get(position)).getDescription());

        TextView txtAddress = convertView.findViewById(R.id.lblAddress);
        txtAddress.setText((listOffice.get(position)).getAddress());

        TextView txtPhone = convertView.findViewById(R.id.lblPhone);
        txtPhone.setText((listOffice.get(position)).getPhone());

        return convertView;
    }
}
