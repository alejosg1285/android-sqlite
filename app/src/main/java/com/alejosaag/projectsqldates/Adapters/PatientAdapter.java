package com.alejosaag.projectsqldates.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.alejosaag.projectsqldates.Entities.Patient;
import com.alejosaag.projectsqldates.R;

import java.util.ArrayList;

public class PatientAdapter extends BaseAdapter {
    private ArrayList<Patient> listPatients;
    private LayoutInflater layoutInflater;
    Context context;
    LayoutInflater inflater;

    public PatientAdapter(Context context, ArrayList<Patient> patients) {
        this.listPatients = patients;
        layoutInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return this.listPatients.size();
    }

    @Override
    public Object getItem(int position) {
        return listPatients.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = layoutInflater.inflate(R.layout.items_listview_patients, null);

        TextView txtId = convertView.findViewById(R.id.lblIdPatient);
        txtId.setText(String.valueOf((this.listPatients.get(position)).getId()));

        TextView txtIdentification = convertView.findViewById(R.id.lblIdentificationPatient);
        txtIdentification.setText((this.listPatients.get(position)).getIdentificacion());

        TextView txtFullName = convertView.findViewById(R.id.lblFullNamePatient);
        txtFullName.setText((this.listPatients.get(position)).getFullName());

        return convertView;
    }
}
