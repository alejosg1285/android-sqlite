package com.alejosaag.projectsqldates;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.alejosaag.projectsqldates.Entities.Patient;
import com.alejosaag.projectsqldates.Models.DPatient;

public class EditPatient extends AppCompatActivity {
    EditText txtIdentification;
    EditText txtName;
    EditText txtLastName;
    Button btnEdit;
    Button btnDelete;
    Button btnMainMenu;

    Patient selectedPatient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_patient);

        selectedPatient = (Patient)getIntent().getSerializableExtra("selectedPatient");

        txtIdentification = findViewById(R.id.txtIdentificationPatient);
        txtName = findViewById(R.id.txtNamePatient);
        txtLastName = findViewById(R.id.txtLastNamePatient);

        btnEdit = findViewById(R.id.btnEditPatient);
        btnDelete = findViewById(R.id.btnDeletePatient);
        btnMainMenu = findViewById(R.id.btnMainMenuPatient);

        txtIdentification.setText(selectedPatient.getIdentificacion());
        txtName.setText(selectedPatient.getNombres());
        txtLastName.setText(selectedPatient.getApellidos());

        btnEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editPatient();
            }
        });
        btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deletePatient();
            }
        });
        btnMainMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToMainMenu();
            }
        });
    }

    private void goToMainMenu() {
        Intent intent = new Intent(getBaseContext(), MainActivity.class);
        startActivity(intent);
    }

    public void deletePatient() {
        int idSelectedPatient = selectedPatient.getId();

        DPatient dPatient = new DPatient(getBaseContext());

        if (dPatient.deletePatient(idSelectedPatient) > 0) {
            Toast.makeText(getBaseContext(), "Paciente eliminado", Toast.LENGTH_LONG).show();
            finish();
        } else {
            Toast.makeText(getBaseContext(), "Error al eliminar el paciente", Toast.LENGTH_LONG).show();
        }
    }

    public void editPatient() {
        int idSelectedPatient = selectedPatient.getId();

        Patient patient = new Patient();

        patient.setId(idSelectedPatient);
        patient.setIdentificacion(txtIdentification.getText().toString());
        patient.setNombres(txtName.getText().toString());
        patient.setApellidos(txtLastName.getText().toString());

        DPatient dPatient = new DPatient(getBaseContext());

        if (dPatient.updatePatient(patient) > 0) {
            Toast.makeText(this, "Registro actualizado", Toast.LENGTH_SHORT).show();
            finish();
        } else {
            Toast.makeText(this, "Error al actualizar registro", Toast.LENGTH_LONG).show();
        }
    }
}
