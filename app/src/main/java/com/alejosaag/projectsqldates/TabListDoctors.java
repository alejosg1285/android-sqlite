package com.alejosaag.projectsqldates;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.alejosaag.projectsqldates.Adapters.DoctorAdapter;
import com.alejosaag.projectsqldates.Entities.Doctor;
import com.alejosaag.projectsqldates.Models.DDoctor;

import java.util.ArrayList;

public class TabListDoctors extends Fragment {
    ListView listDoctors;
    View layout = null;
    Button btnBuscar;
    EditText txtIdentification;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        layout = inflater.inflate(R.layout.tab_list_doctors, container, false);
        listDoctors = layout.findViewById(R.id.lstDoctors);
        btnBuscar = layout.findViewById(R.id.btnBuscar);
        txtIdentification = layout.findViewById(R.id.txtIdentification);

        loadDoctorsList();

        btnBuscar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchDoctor();
            }
        });

        listDoctors.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Doctor doctor = (Doctor)listDoctors.getItemAtPosition(position);

                Intent intent = new Intent(getContext(), EditDoctor.class);
                intent.putExtra("selectedDoctor", doctor);
                startActivity(intent);
            }
        });

        return layout;
    }

    private void loadDoctorsList() {
        DDoctor dDoctor = new DDoctor(getContext());

        ArrayList<Doctor> lstDoctors = new ArrayList<Doctor>();
        lstDoctors = dDoctor.getDoctors();

        DoctorAdapter adapter = new DoctorAdapter(getContext(), lstDoctors);

        listDoctors.setAdapter(adapter);
    }

    @Override
    public void onResume() {
        super.onResume();

        loadDoctorsList();
    }

    private void searchDoctor() {
        DDoctor dDoctor = new DDoctor(getContext());

        String identification = this.txtIdentification.getText().toString();

        if (identification.trim().equals("")) {
            Toast.makeText(getContext(), "Por favor ingrese un número de identificación", Toast.LENGTH_SHORT).show();

            return;
        }

        Doctor doctor = new Doctor();

        doctor = dDoctor.getDoctorByIdentification(identification);

        if (doctor != null) {
            ArrayList<Doctor> lstDoctors = new ArrayList<>();

            lstDoctors.add(doctor);

            DoctorAdapter adapter = new DoctorAdapter(getContext(), lstDoctors);

            listDoctors.setAdapter(adapter);
        } else {
            Toast.makeText(getContext(), "No se encontro el doctor que buscas", Toast.LENGTH_SHORT).show();
        }
    }
}
