package com.alejosaag.projectsqldates;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import com.alejosaag.projectsqldates.Adapters.AppointmentAdapter;
import com.alejosaag.projectsqldates.Entities.Appointment;
import com.alejosaag.projectsqldates.Models.DAppointment;

import java.util.ArrayList;

public class TabListAppointments extends Fragment {
    ListView listAppointments;
    View layout;
    Button btnSearchByDoctor;
    Button btnSearchByPatient;
    EditText txtDoctor;
    EditText txtPatient;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        layout = inflater.inflate(R.layout.tab_list_appointments, container, false);

        listAppointments = layout.findViewById(R.id.listAppointments);
        btnSearchByDoctor = layout.findViewById(R.id.btnSearchByName);
        btnSearchByPatient = layout.findViewById(R.id.btnSearchByPatient);
        txtDoctor = layout.findViewById(R.id.txtDoctorName);
        txtPatient = layout.findViewById(R.id.txtIdentificationPatient);

        loadAppointmentsList();

        listAppointments.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Appointment appointment = (Appointment)listAppointments.getItemAtPosition(position);

                Intent intent = new Intent(getContext(), EditAppointment.class);
                intent.putExtra("appointmentSelected", appointment);
                startActivity(intent);
            }
        });

        return layout;
    }

    private void loadAppointmentsList() {
        DAppointment dAppointment = new DAppointment(getContext());

        ArrayList<Appointment> listAppointments = new ArrayList<>();
        listAppointments = dAppointment.getAppointments();

        AppointmentAdapter adapter= new AppointmentAdapter(listAppointments, getContext());
        this.listAppointments.setAdapter(adapter);
    }

    @Override
    public void onResume() {
        super.onResume();

        loadAppointmentsList();
    }

    private void searchAppointment(int search) {
        DAppointment dAppointment = new DAppointment(getContext());

        ArrayList<Appointment> listAppointments = new ArrayList<>();
        String searchBy;
        if (search == 1) {
            searchBy = txtDoctor.getText().toString();

            if (searchBy.trim().equals("")) {
                Snackbar.make(layout, "Por favor ingrese el nombre del doctor", Snackbar.LENGTH_LONG).show();
                return;
            }

            listAppointments = dAppointment.getAppointmentByDoctorName(searchBy);

            if (listAppointments == null) {
                Snackbar.make(layout, "No se encontraron citas del doctor", Snackbar.LENGTH_LONG).show();
            }
        } else if (search == 2) {
            searchBy = txtPatient.getText().toString();

            if (searchBy.trim().equals("")) {
                Snackbar.make(layout, "Por favor ingrese el número de identificación del paciente", Snackbar.LENGTH_LONG).show();
                return;
            }

            listAppointments = dAppointment.getAppointmentByPatientIdentification(searchBy);

            if (listAppointments == null) {
                Snackbar.make(layout, "No se encontraron citas del paciente", Snackbar.LENGTH_LONG).show();
            }
        }

        AppointmentAdapter adapter = new AppointmentAdapter(listAppointments, getContext());

        this.listAppointments.setAdapter(adapter);
    }
}
