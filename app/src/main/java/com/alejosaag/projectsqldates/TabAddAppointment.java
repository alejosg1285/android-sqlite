package com.alejosaag.projectsqldates;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import com.alejosaag.projectsqldates.Entities.Appointment;
import com.alejosaag.projectsqldates.Entities.Doctor;
import com.alejosaag.projectsqldates.Entities.Office;
import com.alejosaag.projectsqldates.Entities.Patient;
import com.alejosaag.projectsqldates.Models.DAppointment;
import com.alejosaag.projectsqldates.Models.DDoctor;
import com.alejosaag.projectsqldates.Models.DOffice;
import com.alejosaag.projectsqldates.Models.DPatient;

import java.util.ArrayList;

public class TabAddAppointment extends Fragment {
    Button btnAdd;
    Spinner cboPatient;
    Spinner cboDoctor;
    Spinner cboOffice;
    EditText txtDate;
    EditText txtHour;
    View layout;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        layout = inflater.inflate(R.layout.tab_add_appointment, container, false);

        cboDoctor = layout.findViewById(R.id.cboDoctor);
        cboOffice = layout.findViewById(R.id.cboOffice);
        cboPatient = layout.findViewById(R.id.cboPatient);
        txtDate = layout.findViewById(R.id.txtDate);
        txtHour = layout.findViewById(R.id.txtHour);
        btnAdd = layout.findViewById(R.id.btnAdd);

        this.loadSpinnerDoctors();
        this.loadSpinnerPatients();
        this.loadSpinnerOffices();

        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveAppointment();
            }
        });

        return layout;
    }

    private void saveAppointment() {
        DAppointment dAppointment = new DAppointment(getContext());
        Appointment appointment = new Appointment();

        appointment.setDoctorId(((Doctor) cboDoctor.getSelectedItem()).getId());
        appointment.setPatientId(((Patient) cboPatient.getSelectedItem()).getId());
        appointment.setOfficeId(((Office) cboOffice.getSelectedItem()).getId());
        appointment.setDate(txtDate.getText().toString());
        appointment.setHour(txtHour.getText().toString());

        String msg;
        if (dAppointment.insertAppointment(appointment) > 0)
            msg = "Se ha registrado la cita";
        else
            msg = "Error al registrar la cita";

        Snackbar.make(layout, msg, Snackbar.LENGTH_LONG).show();
    }

    private void loadSpinnerOffices() {
        DOffice dOffice = new DOffice(getContext());

        ArrayList<Office> listOffices = new ArrayList<>();
        listOffices = dOffice.getOffices();

        ArrayAdapter<Office> adapter = new ArrayAdapter<>(getContext(), android.R.layout.simple_spinner_dropdown_item, listOffices);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        cboOffice.setAdapter(adapter);
    }

    private void loadSpinnerPatients() {
        DPatient dPatient = new DPatient(getContext());

        ArrayList<Patient> listPatients = new ArrayList<>();
        listPatients = dPatient.getPatients();

        ArrayAdapter<Patient> adapter = new ArrayAdapter<>(getContext(), android.R.layout.simple_spinner_dropdown_item, listPatients);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        cboPatient.setAdapter(adapter);
    }

    private void loadSpinnerDoctors() {
        DDoctor dDoctor = new DDoctor(getContext());

        ArrayList<Doctor> listDoctors = new ArrayList<>();
        listDoctors = dDoctor.getDoctors();

        ArrayAdapter<Doctor> adapter = new ArrayAdapter<>(getContext(), android.R.layout.simple_spinner_dropdown_item, listDoctors);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        cboDoctor.setAdapter(adapter);
    }
}
