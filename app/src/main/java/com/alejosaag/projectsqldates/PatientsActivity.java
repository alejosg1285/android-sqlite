package com.alejosaag.projectsqldates;

import android.support.v4.app.FragmentTabHost;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class PatientsActivity extends AppCompatActivity {

    private FragmentTabHost tabHost;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_patients);

        tabHost = findViewById(R.id.tabHostPatient);
        tabHost.setup(this, getSupportFragmentManager(), android.R.id.tabcontent);
        tabHost.addTab(tabHost.newTabSpec("tab1").setIndicator("Listado"), TabListPatients.class, null);
        tabHost.addTab(tabHost.newTabSpec("tab2").setIndicator("Agregar"), TabAddPatient.class, null);
    }
}
