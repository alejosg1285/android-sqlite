package com.alejosaag.projectsqldates.Entities;

import java.io.Serializable;

public class Patient implements Serializable {
    private int id;
    private String identificacion;
    private String nombres;
    private String apellidos;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getIdentificacion() {
        return identificacion;
    }

    public void setIdentificacion(String identificacion) {
        this.identificacion = identificacion;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getFullName() {
        return String.format("%s %s", nombres, apellidos);
    }

    @Override
    public String toString() {
        return String.format("%s %s", nombres, apellidos);
    }
}
