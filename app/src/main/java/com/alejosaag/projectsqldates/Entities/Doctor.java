package com.alejosaag.projectsqldates.Entities;

import java.io.Serializable;

public class Doctor implements Serializable {
    private int id;
    private String identificacion;
    private String nombres;
    private String apellidos;
    private int especialidad_id;
    private String descEsp;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getIdentificacion() {
        return identificacion;
    }

    public void setIdentificacion(String identificacion) {
        this.identificacion = identificacion;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public int getEspecialidad_id() {
        return especialidad_id;
    }

    public void setEspecialidad_id(int especialidad_id) {
        this.especialidad_id = especialidad_id;
    }

    public String getDescEsp() {
        return descEsp;
    }

    public void setDescEsp(String descEsp) {
        this.descEsp = descEsp;
    }

    @Override
    public String toString() {
        return String.format("%s %s", nombres, apellidos);
    }
}
